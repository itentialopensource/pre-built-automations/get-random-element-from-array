<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 01-15-2024 and will be end of life on 01-15-2025. The capabilities of this Pre-Built have been replaced by the [IAP - Data Manipulation](https://gitlab.com/itentialopensource/pre-built-automations/iap-data-manipulation)

# Get Random Element From Array

## Table of Contents

*  [Overview](#overview)
*  [Installation Prerequisites](#installation-prerequisites)
*  [How to Install](#how-to-install)
*  [How to Run](#how-to-run)
*  [Attributes](#attributes)

## Overview

This JST allows IAP users to get a random element from an array. The attributes returned by the JST include the random element, the index at which the element existed, the array without the random element, and the length of the array without the random element.

## Installation Prerequisites
Users must satisfy the following prerequisites:
* Itential Automation Platform : `^2022.1`


## How to Install

To install the pre-built:
* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Prerequisites](#installation-prerequisites) section.
* The pre-built can be installed from within `App-Admin_Essential`. Simply search for the name of your desired pre-built and click the install button.
 

## How to Run

Use the following to run the pre-built:
1. Once the JST is installed as outlined in the [How to Install](#how-to-install) section above, navigate to the workflow where you would like to get a random element from an array and add a `JSON Transformation` task.

2. Inside the `Transformation` task, search for and select `randomElementFromArray` (the name of the internal JST).

3. The input to the JST is an array from which you want to get a random element.

4. Save your input and the task is ready to run inside of IAP.

## Attributes

Attributes for the pre-built are outlined in the following tables.

**Input**
<table border='1' style='border-collapse:collapse'>
<thead>
<tr>
<th>Attribute</th>
<th>Description</th>
<th>Type</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>array</code></td>
<td>Array from which a random element has to be selected.</td>
<td><code>array</td>
</tr>
</tbody>
</table>

<br>

**Output**
<table border='1' style='border-collapse:collapse'>
<thead>
<tr>
<th>Attribute</th>
<th>Description</th>
<th>Type</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>arrayWithoutElement</code></td>
<td>Array without the randomly selected element.</td>
<td><code>array</td>
</tr>
<tr>
<td><code>randomElement</code></td>
<td>Selected random element.</td>
<td><code>array/boolean/number/integer/string/object/null</code></td>
</tr>
<tr>
<td><code>randomElementIndex</code></td>
<td>Index at which the element existed.</td>
<td><code>integer</code></td>
</tr>
<tr>
<td><code>poppedArrayLength</code></td>
<td>Length of the array without the randomly selected element.</td>
<td><code>integer</code></td>
</tr>
</tbody>
</table>
