
## 0.0.12 [01-30-2024]

* make changes for deprecation

See merge request itentialopensource/pre-built-automations/get-random-element-from-array!9

---

## 0.0.11 [05-24-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/get-random-element-from-array!8

---

## 0.0.10 [06-02-2022]

* Patch/dsup 1361

See merge request itentialopensource/pre-built-automations/get-random-element-from-array!7

---

## 0.0.9 [12-03-2021]

* Certify on 2021.2 - Update README.md

See merge request itentialopensource/pre-built-automations/get-random-element-from-array!6

---

## 0.0.8 [07-02-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/get-random-element-from-array!5

---

## 0.0.7 [06-11-2021]

* Certify on IAP 2021.1

See merge request itentialopensource/pre-built-automations/get-random-element-from-array!4

---

## 0.0.6 [12-23-2020]

* Update package.json

See merge request itentialopensource/pre-built-automations/get-random-element-from-array!3

---

## 0.0.5 [07-31-2020]

* Update package.json

See merge request itentialopensource/pre-built-automations/get-random-element-from-array!3

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n
